package sheridan;

import java.util.regex.Pattern;

/*
 * Login Validator class
 * Author: Divjot Chawla
 */
public class LoginValidator {

  public static boolean isValidLoginName( String loginName ) {
    Boolean isValid = false;

    if(loginName.length() >= 6) {

      if(Pattern.matches("^[a-zA-Z0-9]*$",  loginName)) {

        if(Character.isDigit(loginName.charAt(0))) {
          throw new NumberFormatException("Login Name cannot start with a number!");
	                    } 

	                    else {
          isValid = true;
	                    }
	            } 

	            else {
        throw new NumberFormatException("Login Name should start with a number!");
	            }

	            }
	            else {
      throw new NumberFormatException("Login username should be atleast six or more chracters!");
	            }
    return isValid;    }

	}
	

