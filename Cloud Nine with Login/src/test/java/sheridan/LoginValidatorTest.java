package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * Test class
 * Author: Divjot Chawla
 */
public class LoginValidatorTest {

	    @Test
	    public void testIsValidLoginRegular( ) {
	        assertTrue("Invalid Login" , LoginValidator.isValidLoginName("divjot"));
	    }

	    @Test(expected=NumberFormatException.class)
	    public void testIsValidLoginException() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("div"));
	    }

	    @Test
	    public void testIsValidLoginBoundaryIn() {
	        assertTrue("Invalid Login", LoginValidator.isValidLoginName("gurbani"));
	    }

	    @Test
	    public void testIsValidLoginWithNumbersOnBoundaryIn() {
	        assertTrue("Invalid Login", LoginValidator.isValidLoginName("div987"));
	    }

	    @Test
	    public void testIsValidLoginNotStartNumberBoundaryIn() {
	        assertTrue("Invalid Login", LoginValidator.isValidLoginName("d8vjot"));
	    }

	    @Test(expected=NumberFormatException.class)
	    public void testIsValidLoginBoundaryOut() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("divo1"));
	    }

	    @Test(expected=NumberFormatException.class)
	    public void testIsValidLoginAlphaNumbersOnlyBoundaryOut() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("div!ot"));
	    }

	    @Test(expected=NumberFormatException.class)
	    public void testIsValidLoginNotStartWithNumberBoundaryOut() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("9divjot"));
	    }

	    @Test(expected=NumberFormatException.class)
	    public void testIsValidLoginSixAlphaNumericBoundaryOut() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("div12"));
	    }


	}
